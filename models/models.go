package models

type News struct {
	Id          int64  `db:"id" json:"id"`
	Title       string `db:"title" json:"title"`
	Description string `db:"description" json:"description"`
	Link        string `db:"link" json:"link"`
	Img         string `db:"img" json:"img"`
}
