package crawler

import (
	"database/sql"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
)

type News struct {
	Title       string
	Description string
	Link        string
	Img         string
}

func crawlerInform(db *sql.DB) {
	// Make an HTTP GET request to the URL
	res, err := http.Get("https://www.inform.kz/en/lenta")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	// Check the status code of the response
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Select the news items from the HTML document
	var news []News
	doc.Find(".anounce-news").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".anounce-news__name").Text()
		description := s.Find(".anounce-news__text").Text()
		link, _ := s.Find(".anounce-news__link").Attr("href")
		img, _ := s.Find("img").Attr("src")
		news = append(news, News{title, description, link, img})
	})

	// Print the news information
	for _, item := range news {
		stmt, err := db.Prepare("INSERT INTO news (title, description, link, img) VALUES (?, ?, ?, ?)")
		if err != nil {
			log.Println(err)
		}
		res, err := stmt.Exec(item.Title, item.Description, "https://www.inform.kz"+item.Link, item.Img)
		if err != nil {
			log.Println(err)
		} else {
			lastID, err := res.LastInsertId()
			if err != nil {
				log.Println(err)
			}
			rowCnt, err := res.RowsAffected()
			if err != nil {
				log.Println(err)
			}
			fmt.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
		}

	}
}

func crawlerInformAllPages(db *sql.DB) {
	// Make an HTTP GET request to the URL
	for i := 100; i > 0; i-- {
		s := fmt.Sprintf("https://www.inform.kz/en/lenta/%d", i)
		res, err := http.Get(s)
		if err != nil {
			log.Fatal(err)
		}
		defer res.Body.Close()

		// Check the status code of the response
		if res.StatusCode != 200 {
			log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
		}

		// Load the HTML document
		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			log.Fatal(err)
		}

		// Select the news items from the HTML document
		var news []News
		doc.Find(".anounce-news").Each(func(i int, s *goquery.Selection) {
			title := s.Find(".anounce-news__name").Text()
			description := s.Find(".anounce-news__text").Text()
			link, _ := s.Find(".anounce-news__link").Attr("href")
			img, _ := s.Find("img").Attr("src")
			news = append(news, News{title, description, link, img})
		})

		// Print the news information
		for _, item := range news {
			stmt, err := db.Prepare("INSERT INTO news (title, description, link, img) VALUES (?, ?, ?, ?)")
			if err != nil {
				log.Println(err)
			}
			res, err := stmt.Exec(item.Title, item.Description, "https://www.inform.kz"+item.Link, item.Img)
			if err != nil {
				log.Println(err)
			} else {
				lastID, err := res.LastInsertId()
				if err != nil {
					log.Println(err)
				}
				rowCnt, err := res.RowsAffected()
				if err != nil {
					log.Println(err)
				}
				fmt.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
			}
		}
	}
}
