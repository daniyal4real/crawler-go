package config

import "github.com/spf13/viper"

type Config struct {
	Port        string
	MySqlSource string `mapstructure:"MYSQL_SOURCE"`
}

func NewConfig() (*Config, error) {
	viper.SetConfigFile(".env")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	var config Config
	err = viper.Unmarshal(&config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
