package mapping

import (
	controller "crawler-go/controllers"
	"database/sql"
	"github.com/gin-gonic/gin"
)

var Router *gin.Engine

func CreateUrlMappings(db *sql.DB) {
	c := controller.NewController(db)
	Router = gin.Default()
	Router.Use(controller.Cors())
	// v1 of the API
	v1 := Router.Group("/v1")
	{
		v1.GET("/news", c.GetNewsByPage)
		v1.GET("/news/:id", c.GetNewsByPage)
		v1.POST("/news", c.CreateNews)
		v1.GET("/news/search/:keyword", c.GetNewsByKeywords)
		v1.DELETE("/news/:id", c.DeleteNews)
	}
}
